{{ config(materialized='table')}}

with contacts as (
    select 
        concat(email,name) AS contact_email_name 
        from  mehak_intercom._contacts
)

select *
from contacts
